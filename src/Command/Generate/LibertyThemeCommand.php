<?php

/**
 * @file
 * Contains \Drupal\liberty\Command\Generate\LibertyThemeCommand.
 */

namespace Drupal\liberty\Command\Generate;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Command\ThemeRegionTrait;
use Drupal\Console\Command\ThemeBreakpointTrait;
use Drupal\Console\Command\ConfirmationTrait;
use Drupal\Console\Command\GeneratorCommand;
use Drupal\Console\Style\DrupalStyle;
use Drupal\liberty\Generator\LibertyThemeGenerator;

/**
 * Class to add a Drupal Console command that generates a custom theme.
 */
class LibertyThemeCommand extends GeneratorCommand {
  use ConfirmationTrait;
  use ThemeRegionTrait;
  use ThemeBreakpointTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('generate:libtheme')
      ->setDescription($this->trans('commands.generate.theme.description'))
      ->setHelp($this->trans('commands.generate.theme.help'))
      ->addOption(
        'theme',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.theme.options.module')
      )
      ->addOption(
        'machine-name',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.theme.options.machine-name')
      )
      ->addOption(
        'theme-path',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.theme.options.module-path')
      )
      ->addOption(
        'description',
        '',
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.theme.options.description')
      )
      ->addOption(
        'build-tool',
        '',
        InputOption::VALUE_REQUIRED,
        $this->trans('commands.generate.theme.options.build-tool')
      )
      ->addOption(
        'breakpoints',
        '',
        InputOption::VALUE_OPTIONAL,
        $this->trans('commands.generate.theme.options.breakpoints')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $output = new DrupalStyle($input, $output);

    $validators = $this->getValidator();

    // @see use Drupal\Console\Command\ConfirmationTrait::confirmGeneration
    if (!$this->confirmGeneration($output)) {
      return;
    }

    $theme = $validators->validateModuleName($input->getOption('theme'));

    $drupal = $this->getDrupalHelper();
    $drupal_root = $drupal->getRoot();
    $theme_path = $drupal_root . $input->getOption('theme-path');
    $theme_path = $validators->validateModulePath($theme_path, TRUE);

    $machine_name = $validators->validateMachineName($input->getOption('machine-name'));
    $description = $input->getOption('description');
    $build_tool = $input->getOption('build-tool');
    $breakpoints = $input->getOption('breakpoints');
    $skeleton_dirs = $this->getSkeletonDirs();

    $generator = $this->getGenerator();
    $generator->generate(
      $theme,
      $machine_name,
      $theme_path,
      $description,
      $build_tool,
      $breakpoints,
      $skeleton_dirs
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $output = new DrupalStyle($input, $output);

    $string_utils = $this->getStringHelper();
    $validators = $this->getValidator();
    $drupal = $this->getDrupalHelper();
    $drupal_root = $drupal->getRoot();

    try {
      $theme = $input->getOption('theme') ? $this->validateModuleName($input->getOption('theme')) : NULL;
    }
    catch (\Exception $error) {
      $output->error($error->getMessage());

      return;
    }

    if (!$theme) {
      $theme = $output->ask(
        $this->trans('commands.generate.theme.questions.theme'),
        '',
        function ($theme) use ($validators) {
          return $validators->validateModuleName($theme);
        }
      );
      $input->setOption('theme', $theme);
    }

    try {
      $machine_name = $input->getOption('machine-name') ? $this->validateModule($input->getOption('machine-name')) : NULL;
    }
    catch (\Exception $error) {
      $output->error($error->getMessage());

      return;
    }

    if (!$machine_name) {
      $machine_name = $output->ask(
        $this->trans('commands.generate.module.questions.machine-name'),
        $string_utils->createMachineName($theme),
        function ($machine_name) use ($validators) {
          return $validators->validateMachineName($machine_name);
        }
      );
      $input->setOption('machine-name', $machine_name);
    }

    $theme_path = $input->getOption('theme-path');
    if (!$theme_path) {
      $theme_path = $output->ask(
        $this->trans('commands.generate.theme.questions.theme-path'),
        '/themes/custom',
        function ($theme_path) use ($drupal_root, $machine_name) {
          $theme_path = ($theme_path[0] != '/' ? '/' : '') . $theme_path;
          $full_path = $drupal_root . $theme_path . '/' . $machine_name;
          if (file_exists($full_path)) {
            throw new \InvalidArgumentException(
              sprintf(
                $this->trans('commands.generate.theme.errors.directory-exists'),
                $full_path
              )
            );
          }
          else {
            return $theme_path;
          }
        }
      );
      $input->setOption('theme-path', $theme_path);
    }

    $description = $input->getOption('description');
    if (!$description) {
      $description = $output->ask(
        $this->trans('commands.generate.theme.questions.description'),
        'A theme geared towards helping front-end developers set up a Libsass-based theme with modern front-end technologies.'
      );
      $input->setOption('description', $description);
    }

    // --build-tool option.
    $build_tool = $input->getOption('build-tool');
    if (!$build_tool) {
      // Set up options for build tools.
      $build_tool_options = [];
      $build_tool_options[] = 'Grunt';
      $build_tool_options[] = 'Gulp';

      $build_tool = $output->choice(
        'Do you want to use Grunt or Gulp as your front-end build tool',
        $build_tool_options,
        $build_tool_options[0]
      );

      $input->setOption('build-tool', $build_tool);
    }

    // --breakpoints option.
    $breakpoints = $input->getOption('breakpoints');
    if (!$breakpoints) {
      if ($output->confirm(
        $this->trans('commands.generate.theme.questions.breakpoints'),
        FALSE
      )) {
        $breakpoints = $this->breakpointQuestion($output);
        $input->setOption('breakpoints', $breakpoints);
      }
    }
  }

  /**
   * Retrieve instance of Liberty Theme Generator class.
   *
   * @return LibertyThemeGenerator
   *   The LibertyThemeGenerator class.
   */
  protected function createGenerator() {
    return new LibertyThemeGenerator();
  }

  /**
   * Retrieve the directory where theme skeleton files are located.
   *
   * @return string
   *   The skeleton directory location.
   */
  protected function getSkeletonDirs() {
    $skeleton_dirs = __DIR__ . '/../../../console-skeletons';
    return $skeleton_dirs;
  }

}
