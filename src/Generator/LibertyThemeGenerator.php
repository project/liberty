<?php

/**
 * @file
 * Contains \Drupal\liberty\Generator\LibertyThemeGenerator.
 */

namespace Drupal\liberty\Generator;

use Drupal\Console\Generator\Generator;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class to add a Drupal Console generator for creating a custom theme.
 */
class LibertyThemeGenerator extends Generator {

  /**
   * Generate the custom theme based on settings.
   */
  public function generate(
    $theme,
    $machine_name,
    $dir,
    $description,
    $build_tool,
    $breakpoints,
    $skeleton_dirs
  ) {
    $dir .= '/' . $machine_name;
    if (file_exists($dir)) {
      if (!is_dir($dir)) {
        throw new \RuntimeException(
          sprintf(
            'Unable to generate the bundle as the target directory "%s" exists but is a file.',
            realpath($dir)
          )
        );
      }
      $files = scandir($dir);
      if ($files != array('.', '..')) {
        throw new \RuntimeException(
          sprintf(
            'Unable to generate the bundle as the target directory "%s" is not empty.',
            realpath($dir)
          )
        );
      }
      if (!is_writable($dir)) {
        throw new \RuntimeException(
          sprintf(
            'Unable to generate the bundle as the target directory "%s" is not writable.',
            realpath($dir)
          )
        );
      }
    }

    $parameters = array(
      'theme' => $theme,
      'machine_name' => $machine_name,
      'description' => $description,
      'build_tool' => $build_tool,
      'breakpoints' => $breakpoints,
    );

    $kit_files = array(
      'bower.json',
      'README.md',
    );

    $kit_dirs = array(
      'css',
      'js',
      'lint-config',
      'sass',
      'templates',
    );

    $this->copyKitFilesDirs($kit_files, $kit_dirs, $skeleton_dirs, $dir, $machine_name);

    $this->renderFile(
      'starterkit/starter.gitignore.twig',
      $dir . '/' . $machine_name . '.gitignore',
      $parameters
    );

    $this->renderFile(
      'starterkit/info.yml.twig',
      $dir . '/' . $machine_name . '.info.yml',
      $parameters
    );

    $this->renderFile(
      'starterkit/libraries.yml.twig',
      $dir . '/' . $machine_name . '.libraries.yml',
      $parameters
    );

    $this->renderFile(
      'starterkit/theme.twig',
      $dir . '/' . $machine_name . '.theme',
      $parameters
    );

    $this->renderFile(
      'starterkit/package.json.twig',
      $dir . '/' . 'package.json',
      $parameters
    );

    if ($build_tool === 'Gulp') {
      $this->renderFile(
        'optional/gulp/gulpfile.js',
        $dir . '/' . 'gulpfile.js',
        $parameters
      );
    }
    elseif ($build_tool === 'Grunt') {
      $this->renderFile(
        'starterkit/Gruntfile.js',
        $dir . '/' . 'Gruntfile.js',
        $parameters
      );
    }

    if ($breakpoints) {
      $this->renderFile(
        'starterkit/breakpoints.yml.twig',
        $dir . '/' . $machine_name . '.breakpoints.yml',
        $parameters
      );
    }
  }

  /**
   * Retrieve instance of Filesystem class.
   *
   * @return Filesystem
   *   The Filesystem class.
   */
  protected function createFilesystem() {
    return new Filesystem();
  }

  /**
   * Copy files from kit directory into custom theme directory.
   */
  protected function copyKitFilesDirs(array $kit_files, array $kit_dirs, $skeleton_dirs, $dir, $machine_name) {
    $fs = $this->createFilesystem();

    foreach ($kit_files as $kit_file) {
      $fs->copy(
        $skeleton_dirs . '/starterkit/' . $kit_file,
        $dir . '/' . $kit_file
      );
    }

    foreach ($kit_dirs as $kit_dir) {
      $fs->mirror(
        $skeleton_dirs . '/starterkit/' . $kit_dir,
        $dir . '/' . $kit_dir
      );
    }
  }

}
