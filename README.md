# Liberty

Are you a front-end developer looking to get a Drupal 8 theme up and running
using Libsass?

Look no further! Liberty helps to set up your theme to take advantage of modern
front-end technologies.


At heart, Liberty is a theme generator for Drupal. Liberty might eventually have
some preprocess functions and templates for bug fixes. Right now, Liberty is
primarily geared towards letting you generate a custom theme using Drupal
Console.

The default theme Liberty generates aims to provide a straightforward front-end
development setup using libsass, npm, bower and grunt. Stable is the base theme
to keep the default markup as lean as possible. As Liberty develops, more
options may be added in to let the generated custom theme use gulp instead of
grunt, for example. Or to include a pattern lab installation. Or to use a
different grid system instead of Susy.

While more options may be added, the goal will be to focus on a setup that
allows a front-end developer to get up and running quickly to implement a
component-based approach to a custom design.

## Generating a custom theme

Make sure you have Drupal Console installed. Learn more at drupalconsole.org.

Install the Liberty theme, then clear cache. You can now generate a custom
theme with the following command:

```bash
$ drupal generate:libtheme
```

You will be asked a number of questions during the theme generation process.
After you confirm generation, you should be able to find your custom theme in
the /themes/custom directory.

## Custom theme setup

Make sure you have npm, bower and grunt-cli installed

```bash
$ brew install node
$ npm install -g grunt-cli
$ npm install -g bower
```

Then install the theme's dependencies

```bash
$ npm install
$ bower install
```

If the dependencies for a project change, you can update them with:

```bash
$ npm update
$ bower update
```

You can also enable sourcemaps in Gruntfile.js

## Usage

### Wire Dependencies and Compile Sass

```bash
$ grunt
```

### Watch for changes

```bash
$ grunt watch
```

This task will automatically compile Sass when changes are detected in the
`.scss` files.

If you're using ```grunt watch``` and click the [LiveReload](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en)
button, your browser will refresh automatically when Grunt spots a change to
your sass files.

### Autoprefixer

Liberty uses [Autoprefixer](https://github.com/postcss/autoprefixer) to
automatically add browser prefixes to your css properties based on your
configuration. By default, autoprefixer is set to support the last 3 versions of
browsers, and IE9 and up. You can modify this within Gruntfile.js using the the
[syntax documented on the browserlist plugin page](https://github.com/ai/browserslist#queries).

### Wire Dependencies

```bash
$ grunt wiredep
```

This will wire the Bower components specified in ```bower.json``` into
```_vendor.scss``` (See below).

## Dependencies

This will add your externally obtained Sass mixins, such as Susy and Breakpoint,
into your Sass compilation.

```bash
$ bower install <package> --save
$ grunt wiredep
```

Sass files will be added into sass/partials/core/_vendors.scss

```bash
$ bower install susy --save
$ grunt wiredep
```

Where <package> is a registered package, GitHub shorthand (e.g.
" desandro/masonry"), Git endpoint (e.g. "git://github.com/user/package.git") or
a URL (e.g. "http://example.com/script.js"). You can also edit ```bower.json```
directly.

## Installing new Node.js modules

These are typically used for getting Grunt plugins. Either add to package.json
or run:

```bash
$ npm install <module> --save-dev
```

## Credit where credit is due

The architecture for this theme draws heavily upon Lullabot's Windup theme:
https://github.com/Lullabot/windup. Kudos to Sally Young, Carwin Young, Mike
Herchel, Wes Ruvalcaba, Thomas Lattimore, Kris Bulman and the many other
front-end developers at Lullabot who have worked to make Windup a useful starter
theme that helps to save time when kicking off new projects.
