/**
 * @file
 * Gulp file to automate tasks.
 */

(function () {

  'use strict';

  var gulp = require('gulp'),
    del  = require('del');

  /**
   * @task sass
   * Compiles sass files within this theme.
   */
  gulp.task('sass', ['clean:css', 'glob:sass'], function () {
    var sass         = require('gulp-sass'),
        sassLint     = require('gulp-sass-lint'),
        postcss      = require('gulp-postcss'),
        autoprefixer = require('autoprefixer'),
        pxtorem      = require('postcss-pxtorem'),
        csslint      = require('gulp-csslint'),
        processors   = [
          autoprefixer({ browsers: ['last 3 versions', 'ie 9', 'iOS > 6', 'Safari > 6'] }),
          pxtorem({ propWhiteList: [] })
        ];

    gulp.src('sass/*.scss')
      // Lint sass files.
      .pipe(sassLint({'config': 'lint-config/.sass-lint.yml'}))
      .pipe(sassLint.format())
      .pipe(sassLint.failOnError())
      // Process sass.
      .pipe(sass().on('error', sass.logError))
      // Run output through postcss processors.
      .pipe(postcss(processors))
      // One last lint check of final css.
      .pipe(csslint({ lookup: false, defaultFile: 'lint-config/.csslintrc' }))
      .pipe(csslint.reporter())
      // Save final css files.
      .pipe(gulp.dest('./css/'));
  });

  /**
   * @task clean:css
   * Clears out css files that will be recompiled.
   */
  gulp.task('clean:css', function () {
    return del([
      'css/*.css'
    ]);
  });

  /**
   * @task clean:styles
   * Clears out sass glob files that will be recompiled.
   */
  gulp.task('clean:sassglobs', function () {
    return del([
      'sass/partials/global-styles/_components.scss',
      'sass/partials/global-styles/_layouts.scss',
      'sass/partials/global-styles/_shame.scss'
    ]);
  });

  /**
   * @task clean:styles
   * Clears out sass and css files that will be recompiled.
   */
  gulp.task('glob:sass', ['clean:sassglobs'], function () {
    var globSass = require('gulp-sass-globbing'),
        globSassOptions =   { useSingleQuotes: true},
        globSassBase    =     'sass/partials/global-styles',
        globSassFiles   =   {
          '_components.scss': 'components/**/*.scss',
          '_layouts.scss':    'layouts/**/*.scss',
          '_shame.scss':      'shame/**/*.scss'
        };

    for (var target in globSassFiles) {
      if (globSassFiles.hasOwnProperty(target)) {
        var globFiles = globSassFiles[target];
        gulp.src(globFiles, {cwd: globSassBase})
          .pipe(globSass(
            { path: target}, globSassOptions
          ))
          .pipe(gulp.dest(globSassBase));
      }
    }
  });

  /**
   * @task js
   * Processing of js within this theme.
   */
  gulp.task('js', function () {
    var eslint = require('gulp-eslint');

    gulp.src('js/*.js')
      // Run js through eslint.
      .pipe(eslint({ useEslintrc: false, configFile: 'lint-config/.eslintrc' }))
      .pipe(eslint.format())
      .pipe(eslint.failAfterError())
      // Save final js.
      .pipe(gulp.dest('./js/'));
  });

  /**
   * @task wiredep
   * Wire dependencies from Bower into our Sass files.
   */
  gulp.task('wiredep', function () {
    var wiredep    = require('gulp-wiredep'),
        wireconfig = {
          fileTypes: {
            scss: {
              block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
              detect: {
                css: /@import\s['"](.+css)['"]/gi,
                sass: /@import\s['"](.+sass)['"]/gi,
                scss: /@import\s['"](.+scss)['"]/gi
              },
              replace: {
                {% verbatim %}
                css: '@import "{{filePath}}";',
                sass: '@import "{{filePath}}";',
                scss: '@import "{{filePath}}";'
                {% endverbatim %}
              }
            }
          }
        };

    gulp.src('sass/partials/core/variables/_vendors.scss')
      .pipe(wiredep(wireconfig))
      .pipe(gulp.dest('sass/partials/core/variables/_vendors.scss'));
  });

  /**
   * @task sass:watch
   * Watches for changes to sass files and recompiles css when changes are made.
   */
  gulp.task('watch:sass', function () {
    var watch      = require('gulp-watch'),
        livereload = require('gulp-livereload');

    livereload.listen();
    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('css/**/*.*').on('change', livereload.changed);
  });

  /**
   * @task js:watch
   * Watches for changes to js files and recompiles js when changes are made.
   */
  gulp.task('watch:js', function () {
    var watch      = require('gulp-watch'),
        livereload = require('gulp-livereload');

    livereload.listen();
    gulp.watch('js/**/*.js', ['js']);
    gulp.watch('js/**/*.*').on('change', livereload.changed);
  });

  /**
   * Task for watching changes to files and running tasks on change.
   */
  gulp.task('watch', ['watch:sass', 'watch:js']);

  /**
   * Task for handling initial installation of front-end files.
   */
  gulp.task('install', ['wiredep', 'default']);

  /**
   * @task default
   * Take care of default tasks by just typing gulp.
   */
  gulp.task('default',
    [
      'sass',
      'js'
    ]);
})();
