/**
 * @file
 * Grunt file to automate tasks.
 */

module.exports = function (grunt) {

  'use strict';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      options: {
        // Uncomment to use source maps.
        // sourceMap: true
      },
      dist: {
        files: {
          'css/global-styles.css': ['sass/global-styles.scss']
          // Uncomment to provide styles for a particular library.
          // 'css/sample-library-styles.css': ['sass/sample-library-styles.scss']
        }
      }
    },
    sass_globbing: {
      dist: {
        files: {
          'sass/partials/global-styles/_components.scss': 'sass/partials/global-styles/components/**/*.scss',
          'sass/partials/global-styles/_layouts.scss': 'sass/partials/global-styles/layouts/**/*.scss',
          'sass/partials/global-styles/_shame.scss': 'sass/partials/global-styles/shame/**/*.scss'
          // If creating an individual library, make sure to set up Sass
          // globbing for it.
        }
      },
      options: {
        useSingleQuotes: true
      }
    },
    clean: {
      css: [
        'css/*.css',
        'sass/partials/global-styles/_component.scss',
        'sass/partials/global-styles/_layout.scss',
        'sass/partials/global-styles/_shame.scss'
      ]
    },
    wiredep: {
      task: {
        src: [
          'sass/partials/core/variables/_vendors.scss'
        ],
        options: {
          fileTypes: {
            scss: {
              block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
              detect: {
                css: /@import\s['"](.+css)['"]/gi,
                sass: /@import\s['"](.+sass)['"]/gi,
                scss: /@import\s['"](.+scss)['"]/gi
              },
              replace: {
                {% verbatim %}
                css: '@import "{{filePath}}";',
                sass: '@import "{{filePath}}";',
                scss: '@import "{{filePath}}";'
                {% endverbatim %}
              }
            }
          }
        }
      }
    },
    wiredep_create_bower: {
      dist: {}
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({
            browsers: ['last 3 versions', 'ie 9', 'iOS > 6', 'Safari > 6']
          })
        ]
      },
      dist: {
        src: 'css/*.css'
      }
    },
    px_to_rem: {
      dist: {
        files: {
          'css/global-styles.css': ['css/global-styles.css']
          // Uncomment to process px to rem for a particular library's styles.
          // 'css/sample-library-styles.css': ['css/sample-library-styles.css']
        }
      }
    },
    sasslint: {
      options: {
        configFile: 'lint-config/.sass-lint.yml'
      },
      target: ['sass/*.scss']
    },
    csslint: {
      options: {
        csslintrc: 'lint-config/.csslintrc',
        src: ['css/*/.css']
      }
    },
    eslint: {
      options: {
        configFile: 'lint-config/.eslintrc'
      },
      target: ['js/global-scripts.js']
    },
    watch: {
      css: {
        files: ['sass/**/*.scss'],
        tasks: [
          'sass_globbing',
          'sass',
          'postcss:dist',
          'px_to_rem'
        ],
        options: {
          livereload: true,
          spawn: false
        }
      }
    }
  });

  grunt.registerMultiTask('wiredep_create_bower', 'Creates the bower_components directory if it does not exist. This' +
  'stops wiredep from failing when there are no bower packages.', function () {
    var fs = require('fs');
    var path = require('path');
    var bower_config = require('bower-config');
    var cwd = process.cwd();
    var directory = path.join(cwd, (bower_config.read(cwd).directory || 'bower_components'));
    if (!fs.existsSync(directory)) {
      grunt.file.mkdir(directory);
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-sass-globbing');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-wiredep');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-px-to-rem');
  grunt.loadNpmTasks('grunt-sass-lint');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', [
    'clean:css',
    'wiredep_create_bower',
    'wiredep',
    'sass_globbing',
    'sass',
    'postcss:dist',
    'px_to_rem',
    'sasslint',
    'csslint',
    'eslint'
  ]);

};
