# Linters

Config files for CSS, Sass and ES linting. Most of the rules for CSS and ES
linting are based on the rules followed by Drupal core. Using both CSS and Sass
linting may be redundant. You may want to evaluate if the feedback these linters
is useful, or if you can remove the CSS linting.
