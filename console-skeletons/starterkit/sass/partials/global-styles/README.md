# Global Styles

A collection of partials for styles that will be used throughout a
project.

With http/2, there is a much lower penalty for multiple requests. So if
certain components appear only on certain pages, but not others, you may
want to create separate libraries for those components. Some styles,
layouts and components do appear on every single page, and those style
definitions belong within global-styles.

## Partial files

Filename           | Purpose
------------------ | ---------------------------------------------
`_base.scss`       | Pulls together styles for base element partials: should be used sparingly.
`_components.scss` | Sass globbing pulls in styles from individual Sass component partials: no need to edit manually.
`_layouts.scss`    | Sass globbing pulls in styles from individual Sass layout partials: no need to edit manually.
`_shame.scss`      | Sass globbing pulls in styles used for kludges and workarounds to collect all technical debt in one convenient place.

Individual components, layouts, and shame files should be placed in
their corresponding directories. Please see directories for additional
details.

Component, layout and shame partials should not affect each other, so
are fine to pull in with Sass globbing. With base partials, order may
matter, so importing should be done manually.
