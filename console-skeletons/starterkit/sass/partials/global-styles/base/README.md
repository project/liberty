# Base Partials

A collection of architecturally high-level styles for basic elements.
Should be used sparingly.

## Typical contents

Filename          | Purpose
----------------- | ---------------------------------------------
`_reset.scss`     | Eric Meyer's CSS Reset to zero out browser defaults on base elements.
`_root.scss`      | Styles for root elements (e.g. html and body elements).

Be cautious with default styles on elements like headings, links, etc.
Doing so when those default styles are not universal may mean overriding
those styles in multiple components. Those defaults are most useful for
user-generated content where you cannot guarantee what classes will be
used on elements. You may be better off creating a component for
user-generated content, and then setting those default styles scoped to
a class for that type of content.
