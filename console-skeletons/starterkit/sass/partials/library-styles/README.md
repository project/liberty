# Sample Library Styles

A sample of how to collect partials for styles that will be used within
a single library.

With http/2, there is a much lower penalty for multiple requests. So if
certain components appear only on certain pages, but not others, you may
want to create separate libraries for those components. Some styles,
layouts and components do appear on every single page, and those style
definitions belong within global-styles.

## Partial files

Filename           | Purpose
------------------ | ---------------------------------------------
`_components.scss` | Sass globbing pulls in styles from individual Sass component partials: no need to edit manually.
`_layouts.scss`    | Sass globbing pulls in styles from individual Sass layout partials: no need to edit manually.
`_shame.scss`      | Sass globbing pulls in styles used for kludges and workarounds to collect all technical debt in one convenient place.

Individual components, layouts, and shame files should be placed in
their corresponding directories. Please see directories for additional
details.

Component, layout and shame partials should not affect each other, so
are fine to pull in with Sass globbing. Make sure to set up that Sass
globbing within the task runner you are using.
