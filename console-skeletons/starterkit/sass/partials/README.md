# Partials

Top-level organization of Sass partial files.

## Directories

Directory         | Purpose
----------------- | ----------------------------------------------------------------------
`core`            | Functions, mixins, variables and vendor toos: used for global and individual library styles.
`global-styles`   | Base, component and layout styles used globally.
`library-styles`  | Example of how to organize component and layout styles for a single library.

The `core.scss` partial pulls together those items which will be used by
both `global-styles` and individual library styles.
